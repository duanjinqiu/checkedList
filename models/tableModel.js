const mongoose = require('mongoose'),
    tableSchema = require('../schemas/tableSchema')

module.exports = mongoose.model('table', tableSchema);